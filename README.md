# AuditTrail #



### What is AuditTrail? ###

**AuditTrail** is a **Stellar** protocol wallet that incorporates **fee bumps**, **sponsored reserves**, **claimable balances**, and **fine-grained asset authorization** 
while providing sequential records of payments.